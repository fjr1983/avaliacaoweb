package br.com.fiap.mdb.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.fiap.bean.UsuarioBeanLocal;
import br.com.fiap.entity.Usuario;

@WebServlet("/sorteio")
public class ServletMdb extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Resource(mappedName = "java:/queue/AvaliacaoEJBQueue")
	private Queue fila;
	@Resource(mappedName = "java:/ConnectionFactory")
	private ConnectionFactory connectionFactory;
	
    @EJB
    UsuarioBeanLocal usuarioBean;

	public ServletMdb() {}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		PrintWriter out = response.getWriter();
	
	response.setContentType("text/html");
	try {
		
		String cpf = request.getParameter("cpf");
		Connection connection = connectionFactory.createConnection();
		try {
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			MessageProducer messageProducer = session.createProducer(fila);
			
			List<Usuario> todosOsUsuarios = usuarioBean.getAll();
			
			List<Usuario> usuariosNaoSorteados = new ArrayList<Usuario>();
			for(Usuario u : todosOsUsuarios) {
				if(!u.isSorteado() && !u.getCpf().toLowerCase().trim().equals(cpf.toLowerCase().trim())) { // Se ainda não foi sorteado e o CPF é diferente do que o usuário digitou 
					usuariosNaoSorteados.add(u);														 
				}
			}
			
			Usuario usuario = new Usuario();
			if(usuariosNaoSorteados.size() > 0) {
				Random random = new Random();
				int index = random.nextInt(usuariosNaoSorteados.size());					
				usuario = usuariosNaoSorteados.get(index);
				usuario.setSorteado(true);
				usuarioBean.update(usuario);
			}
			else {
				usuario.setNome("Nenhum usuario disponivel");
			}
			
			TextMessage message = session.createTextMessage(); 
			message.setText(usuario.getNome()); 
			messageProducer.send(message);				
			messageProducer.close();    			
			 
		} finally {
			connection.close();
		}
		out.print("<H1>Objeto enviado com sucesso! JMS 1.0</H1>");
	} catch (Exception ex) {
		ex.printStackTrace();
	}		
}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {}
	 
}
