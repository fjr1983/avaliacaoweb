package br.com.fiap.ejbws.client;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.fiap.cadastro.jaxws.Cadastro;
import br.com.fiap.cadastro.jaxws.CadastroLocator;
import br.com.fiap.cadastro.jaxws.CadastroPortType;
import br.com.fiap.cadastro.jaxws.Usuario;

@WebServlet("/cadastro")
public class ServletCadastro extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ServletCadastro() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		String nome = request.getParameter("nome");
		String cpf = request.getParameter("cpf");
		
		Usuario usuario = new Usuario();
		usuario.setNome(nome);
		usuario.setCpf(cpf);
		usuario.setSorteado(false);
		String retorno = null;
		try {
			Cadastro service = new CadastroLocator();
			CadastroPortType port = service.getCadastroPort();
			retorno = port.cadastrar(usuario);
			out.print(retorno);
			out.print("<br/>");

		} catch (Exception e) {

			e.printStackTrace();
		}		
	}
	
	
}
