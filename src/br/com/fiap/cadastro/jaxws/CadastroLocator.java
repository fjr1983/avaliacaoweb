/**
 * CadastroLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.fiap.cadastro.jaxws;

@SuppressWarnings({"serial", "rawtypes", "unchecked"})

public class CadastroLocator extends org.apache.axis.client.Service implements br.com.fiap.cadastro.jaxws.Cadastro {

    public CadastroLocator() {
    }


    public CadastroLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CadastroLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for CadastroPort
    private java.lang.String CadastroPort_address = "http://localhost:8080/AvaliacaoEJBNew/Cadastro/CadastroBean";

    public java.lang.String getCadastroPortAddress() {
        return CadastroPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String CadastroPortWSDDServiceName = "CadastroPort";

    public java.lang.String getCadastroPortWSDDServiceName() {
        return CadastroPortWSDDServiceName;
    }

    public void setCadastroPortWSDDServiceName(java.lang.String name) {
        CadastroPortWSDDServiceName = name;
    }

    public br.com.fiap.cadastro.jaxws.CadastroPortType getCadastroPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CadastroPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCadastroPort(endpoint);
    }

    public br.com.fiap.cadastro.jaxws.CadastroPortType getCadastroPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            br.com.fiap.cadastro.jaxws.CadastroSoapBindingStub _stub = new br.com.fiap.cadastro.jaxws.CadastroSoapBindingStub(portAddress, this);
            _stub.setPortName(getCadastroPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCadastroPortEndpointAddress(java.lang.String address) {
        CadastroPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
	public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (br.com.fiap.cadastro.jaxws.CadastroPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                br.com.fiap.cadastro.jaxws.CadastroSoapBindingStub _stub = new br.com.fiap.cadastro.jaxws.CadastroSoapBindingStub(new java.net.URL(CadastroPort_address), this);
                _stub.setPortName(getCadastroPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CadastroPort".equals(inputPortName)) {
            return getCadastroPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://jaxws.cadastro.fiap.com.br", "Cadastro");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://jaxws.cadastro.fiap.com.br", "CadastroPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("CadastroPort".equals(portName)) {
            setCadastroPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
