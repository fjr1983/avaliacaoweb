/**
 * CadastroPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.fiap.cadastro.jaxws;

public interface CadastroPortType extends java.rmi.Remote {
    public java.lang.String cadastrar(br.com.fiap.cadastro.jaxws.Usuario arg0) throws java.rmi.RemoteException;
}
