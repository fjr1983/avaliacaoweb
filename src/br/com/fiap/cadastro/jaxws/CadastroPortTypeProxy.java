package br.com.fiap.cadastro.jaxws;

public class CadastroPortTypeProxy implements br.com.fiap.cadastro.jaxws.CadastroPortType {
  private String _endpoint = null;
  private br.com.fiap.cadastro.jaxws.CadastroPortType cadastroPortType = null;
  
  public CadastroPortTypeProxy() {
    _initCadastroPortTypeProxy();
  }
  
  public CadastroPortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initCadastroPortTypeProxy();
  }
  
  private void _initCadastroPortTypeProxy() {
    try {
      cadastroPortType = (new br.com.fiap.cadastro.jaxws.CadastroLocator()).getCadastroPort();
      if (cadastroPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)cadastroPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)cadastroPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (cadastroPortType != null)
      ((javax.xml.rpc.Stub)cadastroPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public br.com.fiap.cadastro.jaxws.CadastroPortType getCadastroPortType() {
    if (cadastroPortType == null)
      _initCadastroPortTypeProxy();
    return cadastroPortType;
  }
  
  public java.lang.String cadastrar(br.com.fiap.cadastro.jaxws.Usuario arg0) throws java.rmi.RemoteException{
    if (cadastroPortType == null)
      _initCadastroPortTypeProxy();
    return cadastroPortType.cadastrar(arg0);
  }
  
  
}